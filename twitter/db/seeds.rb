# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

@hugo = User.create(name: 'Bodnar', first_name: 'Hugo', img_url: 'homer.jpg')
@roro = User.create(name: 'Bourdon', first_name: 'Romain', img_url: 'bourdon.jpeg')
User.create(name: 'Serra', first_name: 'Mathieu', img_url: 'serra.jpeg')
User.create(name: 'Gogh', first_name: 'Alteraz', img_url: 'alteraz.jpg')

Tweet.create(user_id: 1, content: 'Bonjour joyeux contribuables ! #projetTwitter')
Tweet.create(user_id: 2, content: 'Salut à tous c\'est Roro ! #projetRuby')
Tweet.create(user_id: 2, content: 'Salut à tous c\'est encore Roro ! #projetRuby')
Tweet.create(user_id: 2, content: 'Salut à tous c\'est toujours Roro ! #projetRuby')
Tweet.create(user_id: 3, content: 'Tweeeet ! #CASIR')
Tweet.create(user_id: 4, content: 'Bonjour tout le monde ! #CASIR')

Follow.create(user: @hugo, followed: 2)
Follow.create(user: @roro, followed: 1)
