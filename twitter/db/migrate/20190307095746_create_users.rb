class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :first_name
      t.string :img_url

      t.timestamps null: false
    end
  end
end
