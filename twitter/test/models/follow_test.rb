require 'test_helper'

class FollowTest < ActiveSupport::TestCase
    def setup
        @user = User.new(name: 'Doe', first_name: 'John')
      end

    test 'invalid follow without followed' do
      follow = Follow.new(user: @user)
      follow.followed = nil
      assert_not_nil follow.errors[:content]
    end
end
