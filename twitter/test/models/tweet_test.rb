require 'test_helper'

class TweetTest < ActiveSupport::TestCase
    def setup
        @user = User.new(name: 'Doe', first_name: 'John')
      end

    test 'invalid tweet without content' do
      tweet = Tweet.new(user: @user)
      tweet.content = nil
      assert_not_nil tweet.errors[:content]
    end

    test 'invalid tweet' do
      tweet = Tweet.new(content: 'test')
      assert tweet.valid?
    end
end
