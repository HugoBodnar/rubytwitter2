require 'test_helper'

class UserTest < ActiveSupport::TestCase
    def setup
        @user = User.new(name: 'Doe', first_name: 'John')
      end

      test 'valid user' do
        assert @user.valid?
      end

      test 'invalid without name' do
        @user.name = nil
        refute @user.valid?, 'saved user without a name'
        assert_not_nil @user.errors[:name], 'no validation error for name present'
      end

      test 'invalid without first_name' do
        @user.first_name = nil
        refute @user.valid?
        assert_not_nil @user.errors[:first_name]
      end
end
