class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show
    if session[:current_user]
      @me = User.find(session[:current_user]['id'])
      @is_followed = Follow.find_by user: @me, followed: @user.id
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /users/1
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    Tweet.all.each do |tweet|
        if tweet.user_id == @user.id
            tweet.destroy
        end
    end
    @user.follows.each do |follow|
        follow.destroy
    end
    Follow.all.each do |follow|
        if follow.followed == @user.id
            follow.destroy
        end
    end
    if @user.id == session[:current_user]['id']
        session.delete(:current_user)
    end
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
    end
  end

  def connect
    @user = User.find(params[:id])
    session[:current_user] = @user
    respond_to do |format|
      format.html { redirect_to @user, notice: 'Connexion réussie !' }
    end
  end

  def disconnect
    session.delete(:current_user)
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Déconnexion réussie !' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :first_name, :img_url)
    end
end
