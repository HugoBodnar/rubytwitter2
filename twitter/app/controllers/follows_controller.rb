class FollowsController < ApplicationController
    def create
        @follower = User.find(session[:current_user]['id'])
        @followed = User.find(params[:followed])
        @follow = @follower.follows.create({:user => @follower, :followed => params[:followed]})
        redirect_to user_path(@followed)
    end

    def destroy
        @user = User.find(session[:current_user]['id'])
        @followed = User.find(params[:followed])
        @follow = Follow.find_by user: @user, followed: params[:followed]
        @delete = @user.follows.find(@follow['id'])
        @delete.destroy
        redirect_to user_path(@followed)
    end
end
