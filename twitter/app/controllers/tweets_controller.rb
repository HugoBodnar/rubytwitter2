class TweetsController < ApplicationController
    def create
        @user = User.find(params[:user_id])
        @tweet = @user.tweets.create(tweet_params)
        redirect_to user_path(@user)
    end

    def filActu
        if session[:current_user]
            @tweets = []
            @usersID = []
            @user = User.find(session[:current_user]['id'])
            @user.follows.each do |follow|
                @usersID << follow.followed
            end
            @usersID << session[:current_user]['id']
            Tweet.all.each do |tweet|
                if @usersID.include? tweet.user_id
                    @tweets << {:user => User.find(tweet.user_id), :tweet => tweet}
                end
            end
            @tweets = @tweets.sort {|x, y| y[:tweet].created_at <=> x[:tweet].created_at}

        end
    end

    def tendances
        if session[:current_user]
            @tweets = []
            @all_hashtags = []
            @hashtags = []
            @nb_hashtags = []
            @selected = params[:hashtag]
            Tweet.all.each do |tweet|
                tag = tweet.content[/#[[:alnum:]]*/]
                if tag != nil
                    @all_hashtags << tag
                    if !@hashtags.include? tag
                        @hashtags << tag
                    end
                end
                @tweets << {:user => User.find(tweet.user_id), :tweet => tweet}
            end
            @tweets = @tweets.sort {|x, y| y[:tweet].created_at <=> x[:tweet].created_at}

            @hashtags.each do |tag|
                @nb_hashtags << {:tag => tag, :count => @all_hashtags.count(tag)}
            end
            @nb_hashtags.sort! {|x, y| y[:count] <=> x[:count]}
        end
    end

    private
        def tweet_params
            params.require(:tweet).permit(:content)
        end
end
