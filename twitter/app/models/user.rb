class User < ActiveRecord::Base
    has_many :tweets
    has_many :follows

    validates :name, :presence => true
    validates :first_name, :presence => true
end
