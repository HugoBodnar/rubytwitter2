Se placer dans le dossier "Twitter"

Télécharger le projet (via gitlab ou autre)

Lancer la commande "bundle install --path vendor/bundle"

Lancer la commande "rake db:migrate" pour construire la base de données

Lancer la commande "rake db:seed" pour ajouter des données au projet

Démarrer le serveur avec la commande "bundle exec rails s"

Se rendre à l'adresse localhost:3000 via son navigateur
